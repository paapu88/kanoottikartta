import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:proj4dart/proj4dart.dart' as proj4;
import 'package:geolocator/geolocator.dart';
import 'package:csv/csv.dart';
import 'dart:async';
import 'package:geodesy/geodesy.dart';
import '../classes/piste.dart';
import 'dart:io';
import 'package:flutter/services.dart' show rootBundle;
import '../classes/track.dart';
import 'dart:convert' show utf8;
import 'package:flutter_floating_map_marker_titles_core/controller/fmto_controller.dart';
import 'package:flutter_floating_map_marker_titles_core/model/floating_marker_title_info.dart';
import 'package:flutter_map_floating_marker_titles/flutter_map_floating_marker_titles.dart';

class CustomCrsPage extends StatefulWidget {
  final FileSystemEntity courseName;
  CustomCrsPage({this.courseName = null});
  static const String route = 'custom_crs';

  @override
  _CustomCrsPageState createState() => _CustomCrsPageState();
}

class _CustomCrsPageState extends State<CustomCrsPage> {
  final FMTOOptions fmtoOptions = FMTOOptions(maxTitleLines:5, textSize: 18.0);
  List<Map<dynamic, dynamic>> kanoottiData = [];
  List<Marker> kanoottiMarkers = [];
  List<FloatingMarkerTitleInfo> kanoottiFloatTexts = [];
  Geolocator _geolocator;
  Position _position;
  LatLng oldp;
  LatLng newp;
  Piste piste;
  bool found = false;
  double oldRotation = 0.0;
  double rotation = 0.0;
  double pseudoRotation = null;
  double latitude = 60.192059;
  double longitude = 24.945831;
  Position oldPosition = null;
  Geodesy geodesy = Geodesy();
  double maxZoom = 17;
  Future<Track> futureTrack;
  String initText = 'Map centered to';
  //MapController mapController;
  final FMTOMapController mapController = FMTOMapController();
  //StatefulMapController statefulMapController;
  //StreamSubscription<StatefulMapControllerStateChange> sub;

  bool ready = false;
  bool doCenter = false;
  bool doRotate = false;


  @override
  void initState() {
    //mapController = MapController();
    //statefulMapController = StatefulMapController(mapController: mapController);
    //statefulMapController.onReady.then((_) => setState(() => ready = true));
    //sub = statefulMapController.changeFeed.listen((change) => setState(() {}));
    super.initState();
    //futureTrack = fetchTrack();
    _geolocator = Geolocator();
    loadAsset(); // load kanootti data from csv file
    //piste = Piste(lat: 60.192059, lon: 24.945831);
    LocationOptions locationOptions = LocationOptions(
        accuracy: LocationAccuracy.high,
        distanceFilter: 15, //meters
        timeInterval: 2000); //milliseconds
    StreamSubscription positionStream = _geolocator
        .getPositionStream(locationOptions)
        .listen((Position position) {
      oldPosition = _position;
      _position = position;
      latitude = _position.latitude;
      longitude = _position.longitude;
      setState(() {
        piste = Piste(lat: _position.latitude, lon: _position.longitude);
        //do_rotation();
        //mapController.onRotationChanged(10.0);
      });
    });
  }

  loadAsset() async {
    //final input = new File("assets/kaikkiutf8.csv").openRead();
    //final fields = await input.transform(utf8.decoder).transform(new CsvToListConverter()).toList();
    var myData = await rootBundle.loadString("assets/kaikkiutf8.csv");
    List<List<dynamic>> csvTable = await CsvToListConverter(eol: "\r\n").convert(myData);
    List<Map<dynamic, dynamic>> data = [];
    List<Marker> markers = [];
    List<FloatingMarkerTitleInfo> floattexts = [];
    Map map = {};
    //print(csvTable.length);
    //int l= csvTable[0].length;
    //print(csvTable[0][l-2]);

    //print(csvTable[0][l-1]);

    for(int i=0; i<csvTable.length; i++){
      //print('0: ${csvTable[i][0]}, 1: ${csvTable[i][1]}, 2:${csvTable[i][2]}');
      map = {'lat': csvTable[i][1],
        'lng': csvTable[i][0],
        'info': csvTable[i][2],
      };
      data.add(map);

      //print("$i, ${csvTable[0][i]}");
    }
    //print(data.length);
    int count=0;
    data.forEach((d) {
      //print('${d['lat']}, ${d['lon']}');
      floattexts.add(
          FloatingMarkerTitleInfo(
            id: count,
            latLng: LatLng(d['lat'], d['lng']),
            title: d['info'],
            color: Colors.green,
          )
      );

    count++;
      markers.add(
        Marker(
          width: 40.0,
          height: 40.0,
          anchorPos: AnchorPos.align(AnchorAlign.top),
          point: LatLng(d['lat'], d['lng']),
          builder: (ctx) =>
              Container(
                child: Image.asset(
                  'assets/images/markerGreen.png',
                  fit: BoxFit.fitHeight,
                  width: 14,
                  height: 20,
                ),
              ),
        ),
    );
    });

        //for(final d in data)
    //  print('$d');

    setState(() {
      kanoottiFloatTexts=floattexts;
      kanoottiMarkers=markers;
    });
    //print("kanoottiData:" + kanoottiData.toString());

  }

  void do_rotation() {
    //print("rotate1");
    if (oldPosition != null && _position != null) {
      oldp = LatLng(oldPosition.latitude, oldPosition.longitude);
      newp = LatLng(_position.latitude, _position.longitude);
      //LatLng oldp = LatLng(65.00, 25.00);
      //LatLng newp = LatLng(65.00, 26.00);
      rotation = geodesy.finalBearingBetweenTwoGeoPoints(oldp, newp);
      //print("oldp:" + oldp.toString());
      //print("newpp:" + newp.toString());

      //print("bearing:" + rotation.toString());
      if ((rotation != oldRotation) || (pseudoRotation == 0.0)) {
        //print("rotate2!");
        mapController.rotate(360.0 - rotation);
        oldRotation = rotation;
      }
    }
  }

  LatLng get_point() {
    if (mapController.ready) {
      if (doCenter) {
        if (doRotate) {
          //print("rotating normally");
          do_rotation();
          pseudoRotation = null;
        }
        mapController.move(LatLng(latitude, longitude), mapController.zoom);
      } else {
        // not centering, rotate back to 0 deg
        if (pseudoRotation == null) {
          //print("rotating to zero");
          mapController.rotate(0.0);
          pseudoRotation = 0.0;
        }
      }
    }
    return LatLng(latitude, longitude);
  }

  Future<Position> getLocation() async {
    var currentLocation;
    try {
      currentLocation = await _geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  @override
  Widget build(BuildContext context) {
    final FileSystemEntity args = ModalRoute.of(context).settings.arguments;

    return Scaffold(

      //appBar: AppBar(title: Text('Custom CRS')),
      //drawer: buildDrawer(context, CustomCrsPage.route),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder<Track>(
          future: fetchTrack(args),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              //return FlutterMap(
              return FlutterMapWithFMTO(
                kanoottiFloatTexts,
                fmtoOptions: fmtoOptions,
                mapController: mapController,
                options: MapOptions(
                    // Set the default CRS
                    //crs: epsg3067CRS
                    center: get_point(),
                    //center: userCenter,
                    zoom: maxZoom-2,
                    // Set maxZoom usually scales.length - 1 OR resolutions.length - 1
                    // but not greater
                    maxZoom: maxZoom,
                    //maxZoom: 16.0,
                    onTap: (point) {
                      final snackBar = SnackBar(
                        content: Text(
                            "PAINA PITKÄÄN niin vaihtuu: keskitetty/ei keskitetty kartta "),
                        duration: Duration(seconds: 3),
                        backgroundColor: Colors.red,
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                    },
                    onLongPress: (point) {
                      String teksti;
                      doCenter = !doCenter;
                      doRotate = !doRotate;
                      //print("docenter:" + doCenter.toString());
                      //print("dorotate:" + doCenter.toString());
                      if (doRotate) {
                        teksti = "Valittu: Kartan keskitys+pyöritys";
                      } else {
                        teksti = "Valittu: Kartan liikuttelu+skaalaus";
                      }
                      final snackBar = SnackBar(
                        content: Text(teksti),
                        duration: Duration(seconds: 3),
                        backgroundColor: Colors.blue,
                      );
                      Scaffold.of(context).showSnackBar(snackBar);

                      get_point();
                    }),
                layers: [
                  TileLayerOptions(
                      maxNativeZoom: maxZoom,
                      updateInterval: 1000,
                      urlTemplate:
                          //"https://mtb-tileserver.trailmap.fi/tiles/{z}/{x}/{y}.png",
                          //"https://tilestrata.trailmap.fi/d3-mtbmap-cache/3d/{z}/{x}/{y}@2x.webp",
                        "https://tiles.kartat.kapsi.fi/peruskartta/{z}/{x}/{y}.jpg",
                      //"https://mtb-tileserver.trailmap.fi/tiles@2x/{z}/{x}/{y}.png",
                      //"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c']),
                  //if (userLocation != null) {
                  CircleLayerOptions(
                      //circles: snapshot.data.controls + piste.piste),
                      circles: piste.piste),
                  PolylineLayerOptions(polylines: [
                    Polyline(
                      points: snapshot.data.trackList,
                      isDotted: true,
                      color: Colors.red,
                      strokeWidth: 6.0,
                      //borderColor: Colors.red,
                      //borderStrokeWidth: 0.1,
                    )
                  ]),

                  //}
                  new MarkerLayerOptions(
                    markers: kanoottiMarkers,
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

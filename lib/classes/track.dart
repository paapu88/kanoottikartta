import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:gpx/gpx.dart';
//import 'package:file_picker/file_picker.dart';

class Track {
  final String track;
  var trackList = new List<LatLng>();

  Track({this.track}) {
    Iterable<String> list = LineSplitter.split(track);
    list.forEach((e) {
      if (e.contains("<trkpt")) {
        List<String> cols = e.split("\"");
        //print(cols[1] + cols[3]);
        trackList.add(LatLng(double.parse(cols[1]), double.parse(cols[3])));
      }
    });
    print(trackList);
  }

  factory Track.fromString(String mystring) {
    return Track(track: mystring);
  }
}

Future<String> get _localPath async {
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }
  final directory = await getExternalStorageDirectory();
  return directory.path;
}

//Future<Rastit> fetchRastit({String url='https://map-and-db.herokuapp.com/track/', String name='espookr'}) async {
// final response = await http.get(url + name);
Future<Track> fetchTrack(FileSystemEntity courseName) async {
  //final path = await _localPath;
  if (courseName == null) {
    return Track.fromString("");
  }
  final file = File(courseName.path);
  //final file = await _localFile;
  String contents = await file.readAsString();
  return Track.fromString(contents);
}
